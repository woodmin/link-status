package main

import (
	"link-monitor/internal/app/api/endpoints"
	"log"
	"net/http"
)

func main() {
	var err error
	log.SetFlags(log.Llongfile | log.Ldate | log.Ltime)
	log.Println("start linkmonitor on 127.0.0.1:8000")

	e := endpoints.New()
	http.HandleFunc("/api/events/list/all", e.ListAllEvents)
	http.HandleFunc("/api/events/list/clients/all", e.ListAllByCLient)
	http.HandleFunc("/api/events/new", e.NewEvent)

	err = http.ListenAndServe("127.0.0.1:8000", nil)
	isFatal(err)
}

func isFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
