package endpoints

import "net/http"

type Endpoints struct{}

func New() Endpoints {

	return Endpoints{}
}

func (e *Endpoints) NewEvent(w http.ResponseWriter, r *http.Request) {

}

func (e *Endpoints) DeleteEvent(w http.ResponseWriter, r *http.Request) {

}

func (e *Endpoints) ListAllEvents(w http.ResponseWriter, r *http.Request) {

}

func (e *Endpoints) ListOpenEvents(w http.ResponseWriter, r *http.Request) {

}

func (e *Endpoints) ListAllByCLient(w http.ResponseWriter, r *http.Request) {

}

func (e *Endpoints) ListOpenByCLient(w http.ResponseWriter, r *http.Request) {

}
